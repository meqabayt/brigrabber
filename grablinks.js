const axios = require("axios").default;
const cheerio = require('cheerio');
const fs = require('fs');
const postsJson = require("./postLinks.json");
const TurndownService = require('turndown');
const FormData = require('form-data');

const downloadImage = require("./download.js");


const client = axios.create({
  timeout: 100000
});

// postlarin linklerini file'a yazmag ucun bu funksiyani cagir :P
(async () => {
  // get category urls
  const url = "https://bakuresearchinstitute.org/";
  const links = await getDataFromURL(url, "#menu-main_menu-1 > li.menu-item > a", "href");

  // get post links
  let postLinks = []
  for (let i = 0; i < links.length; i++) {
    await client.get(links[i]).then((res) => {
      const $ = cheerio.load(res.data);
      const postCardElements = $(".bri-custom-post")

      postCardElements.map((i, card) => {
        let cardHtml = cheerio.load(card)
        let postLink = cardHtml(".bri-post-content > a")
        let postThumbnail = cardHtml("img")
        postLinks.push({ link: postLink[0].attribs.href, thumbnail: postThumbnail[0].attribs.src })
      })
    })

  }

  const json = JSON.stringify(postLinks);

  fs.writeFile("postLinks.json", json, 'utf8', function (err) {
      if (err) {
          console.log("An error occured while writing JSON Object to File.");
          return console.log(err);
      }
      
      console.log("JSON file has been saved.");
  });
})()

async function getDataFromURL(url, selector, attr = false) {
  try {
      const res = await getContent(url);
      console.log(res.status, res.statusText)
  
      // get category urls
      let $ = cheerio.load(res.data, {decodeEntities: false})
      let html = $(selector);
      let title = $(".page-item-title-single > h1").html();

      if (!attr)
          return { html: html, title: title }
      const attrs = [];
      html.map((i, item) => attrs.push(item.attribs[attr]))
      return attrs
  } catch (err) {
      console.log("Error:", err)
  }
}


function getContent(url) {
  return client.get(url);
}


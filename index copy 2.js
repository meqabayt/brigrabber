const axios = require("axios").default;
const cheerio = require("cheerio");
const fs = require("fs");
const postsJson = require("./postLinks.json");
const TurndownService = require("turndown");
const FormData = require("form-data");

const downloadImage = require("./download.js");
const stream = axios.create({
  responseType: "stream"
});

const client = axios.create({
  timeout: 100000
});

const strapi = axios.create({
  baseURL: "https://admin.bakuresearchinstitute.org",
  timeout: 10000
});

function getContent(url) {
  return client.get(url);
}

async function getDataFromURL(url, selector, attr = false) {
  try {
    const res = await getContent(url);
    console.log(res.status, res.statusText);

    // get category urls
    let $ = cheerio.load(res.data, { decodeEntities: false });
    let html = $(selector);
    let title = $(".page-item-title-single > h1").html();
    let headerImageSource = $(".with-bg")[0].attribs["data-style"].split(
      "url("
    )[1];
    headerImageSource = headerImageSource.substring(
      0,
      headerImageSource.length - 2
    );
    console.log("headerImage: ", headerImageSource);

    if (!attr)
      return { html: html, title: title, headerImageSource: headerImageSource };
    const attrs = [];
    html.map((i, item) => attrs.push(item.attribs[attr]));
    return attrs;
  } catch (err) {
    console.log("Error:", err);
  }
}

function makeQuery(data) {
  const content = data.content;
  const title = data.title;

  const input = {
    title: JSON.parse(JSON.stringify(title)),
    description: "",
    content: `${content.replace(/"/g, "'").replace(/\/n/g, "")}`,
    draft: true,
    language: "5d611cd73a0dc8301ec1f98e"
  };
  return input;
}

try {
  console.log(postsJson.length);
  for (let i = 0; i < postsJson.length; i++) {
    let azeUrlToGrab = postsJson[i].link.replace(
      "https://bakuresearchinstitute.org",
      "https://bakuresearchinstitute.org/az"
    );
    const thumbnailIMGName = postsJson[i].thumbnail.split("/").pop();

    client({
      method: "get",
      url: postsJson[i].thumbnail,
      responseType: "stream"
    }).then(res => {
      let stream = res.data.pipe(
        fs.createWriteStream(`./images/${thumbnailIMGName}`)
      );
      stream.on("finish", function() {
        uploadPostContent(azeUrlToGrab, undefined, thumbnailIMGName);
      });
    });
  }
} catch (err) {
  console.log("error async", err);
}

function createPost(
  contentString,
  title,
  linkToGrab,
  translationOfID,
  thumbnailImageName,
  headerImageName
) {
  console.log(title, "************`title");
  var turndownService = new TurndownService();
  var markdown = turndownService.turndown(contentString);

  if (translationOfID === undefined) {
    getLangs().then(response => {
      let azLangID = response.data.filter(elem => elem.code == "az")[0]._id;
      strapi
        .post("/posts", {
          title: JSON.parse(JSON.stringify(title)),
          description: "",
          content: `${markdown.replace(/"/g, "'").replace(/\/n/g, "")}`,
          draft: true,
          language: azLangID
        })
        .then(response => {
          let id = response.data._id;
          uploadFileAndGetUrl(
            thumbnailImageName,
            "",
            function() {
              uploadFileAndGetUrl(
                headerImageName,
                "",
                function() {
                  uploadPostContent(
                    linkToGrab.replace(
                      "https://bakuresearchinstitute.org/az/",
                      "https://bakuresearchinstitute.org/"
                    ),
                    id
                  );
                },
                { refID: id, ref: "post", field: "header_image" }
              );
            },
            { refID: id, ref: "post", field: "image" }
          );
        });
    });
  } else {
    getLangs().then(response => {
      let enLangID = response.data.filter(elem => elem.code == "en")[0]._id;
      console.log("+==================");
      console.log("enlangid: ", enLangID);
      console.log("translationOfID: ", translationOfID);

      strapi.post("/posts", {
        title: JSON.parse(JSON.stringify(title)),
        description: "",
        content: `${markdown.replace(/"/g, "'").replace(/\/n/g, "")}`,
        draft: true,
        language: enLangID,
        translations: [translationOfID]
      });
    });
  }
}

function getLangs() {
  return strapi.get("/languages");
}

async function uploadPostContent(
  linkToGrab,
  translationOfID,
  thumbnailImgName
) {
  let grabbedContent = await getDataFromURL(
    linkToGrab,
    ".entry-content.about-page"
  );
  let html = grabbedContent.html;
  let title = grabbedContent.title;
  let headerImageSource = grabbedContent.headerImageSource;
  const headerImgName = headerImageSource.split("/").pop();

  const ch = require("cheerio");

  let content = ch.load(html.html());
  contentString = html.html();

  let imgs = content("img");
  console.log("images length: ", imgs.length);

  client({
    method: "get",
    url: headerImageSource,
    responseType: "stream"
  }).then(res => {
    let stream = res.data.pipe(
      fs.createWriteStream(`./images/${headerImgName}`)
    );
    stream.on("finish", function() {
      imgs.map((i, img) => {
        console.log("img", i);
        const filename = img.attribs.src.split("/").pop();
        client({
          method: "get",
          url: img.attribs.src,
          responseType: "stream"
        }).then(res => {
          let stream = res.data.pipe(
            fs.createWriteStream(`./images/${filename}`)
          );
          stream.on("finish", function() {
            uploadFileAndGetUrl(filename, img.attribs.src, function(paths) {
              contentString = html.html().replace(paths.oldPath, paths.newPath);
              contentString = contentString.replace(
                "srcset=",
                "disabled_srcset="
              );

              if (i + 1 == imgs.length) {
                createPost(
                  contentString,
                  title,
                  linkToGrab,
                  translationOfID,
                  thumbnailImgName,
                  headerImgName
                );
              }
            });
          });
        });
      });

      if (imgs.length == 0) {
        createPost(
          contentString,
          title,
          linkToGrab,
          translationOfID,
          thumbnailImgName,
          headerImgName
        );
      }
    });
  });
}

function uploadFileAndGetUrl(filename, oldPath, callback, refObject) {
  var bodyFormData = new FormData();
  pathToFile = "images/" + filename;
  imageFile = fs.createReadStream(pathToFile);
  bodyFormData.append("files", imageFile);

  if (refObject) {
    bodyFormData.append("refId", refObject.refID);
    bodyFormData.append("ref", refObject.ref);
    bodyFormData.append("field", refObject.field);
  }

  result = client({
    method: "post",
    url: "https://admin.bakuresearchinstitute.org" + "/upload",
    headers: {
      "Content-Type": `multipart/form-data; boundary=${bodyFormData._boundary}`
    },
    data: bodyFormData
  })
    .then(res => {
      callback({
        oldPath: oldPath,
        newPath: res.data[0].url,
        attachmentID: res.data[0]._id
      });
    })
    .catch(err => {
      console.log(err, "err");
    });
}

const axios = require("axios").default;
const cheerio = require('cheerio');
const fs = require('fs');
const TurndownService = require('turndown');


const client = axios.create({
    baseURL: "https://bakuresearchinstitute.org",
    timeout: false
})
const strapi = axios.create({
    baseURL: "http://localhost:1337",
    timeout: false,
    headers: {
        "Content-Type": "application/json; charset=utf-8"
    }
})


const turndownService = new TurndownService();
const langs = ["az", "en"];



// get categories
(async () => {
    const cats = langs.map(lang => {
        return client.get(lang)
            .then(res => getCategories(res.data))
            .then(cat => cat)
    })
    const categories = pairingByLang(await cats[0], await cats[1]);
    const langsFromDB = await getLangs();
    
    let langObj = {}
    langsFromDB.data.map(item => {
        langObj[item.code]={...item}
    })

    categories.map(async (cat, i) => {
        // *** insert tags with translations ***
        // await strapi.post("/tags", {
        //     name: cat[`name_az`],
        //     menu: true,
        //     selected: true,
        //     language: langObj.az._id
        // })
        //     .then(res => {
        //         categories[i].id_az = res.data.id
        //         return res
        //     })
        //     .then(async res => { 
        //         const cat_en = await strapi.post("/tags", {
        //             name: cat[`name_en`],
        //             menu: true,
        //             selected: true,
        //             language: langObj.en._id,
        //             translation_of: res.data._id
        //         })
        //         categories[i].id_en = cat_en.data.id
        //     }).catch(err => console.log(err))

        // *** get posts by tag ***
        await client.get(cat.url_az).then(res => {
            const $ = cheerio.load(res.data);
            
            const postCardHtml = $(".bri-custom-post")
            const postCards = []
            postCardHtml.map((i, card) => {
                const cardHtml = cheerio.load(card)
                const link = cardHtml(".bri-post-content > a")
                const main_images = cardHtml(".bri-post-image")
                const descs = cardHtml(".bri-post-content > a + p")
                link.map((i, a) => {
                    postCards.push({
                        link_az: a.attribs.href,
                        link_en: a.attribs.href.replace("/az",""),
                        image: main_images[i].attribs.src,
                        desc: descs[i].children[0].data.trim()
                    })
                })
            })
            categories[i].postCards = [...postCards]
            
            // const contentMarkdown = turndownService.turndown(content.html())
            // console.log(contentMarkdown)
            // strapi.post("/posts", {
            //     title: title,
            //     content: contentMarkdown,
            //     draft: true,
            //     language: langObj.az._id
            // })
        })

        // console.log(cat)

        cat.postCards.map(async (post, i) => {
            try {
                const res = await client.get(post.link_az);
                const postHtml = cheerio.load(res.data);
                const title = postHtml(".page-item-title-single > h1");
                const content = postHtml(".entry-content.about-page");
                console.log(content[0].children[0].data.trim())
                await strapi.post("/posts", {
                    title: title[0].children[0].data.trim(),
                    description: post.desc,
                    content: turndownService.turndown(content.html())
                })
            } catch (err) {
                console.log(err)
            }
        })

    })
})()


function getLangs() {
    return strapi.get("/languages")
}

function writeToFile(name, content, cb = undefined) {
    fs.writeFile(name, content, 'utf8', function (err) {
        if (err) {
            console.log("An error occured while writing JSON Object to File.");
            return console.log(err);
        }
        console.log("JSON file has been saved.");
        cb && cb()
    });
}

function getCategories(content) {
    const $ = cheerio.load(content)
    const nav = $("#menu-main_menu-1 > li.menu-item > a")
    let category = []

    nav.map((i, item) => {
        category.push({
            name: item.children[0].data,
            url: item.attribs.href
        })
    })
    return category
}

function pairingByLang(az, en) {
    let data = []
    az.map((cat, i) => {
        data.push({
            name_az: cat.name,
            name_en: en[i].name,
            url_az: cat.url,
            url_en: en[i].url
        })
    })
    return data
}

const axios = require("axios").default;
const cheerio = require('cheerio');
const fs = require('fs');
const postsJson = require("./postLinks.json");
const TurndownService = require('turndown');
const FormData = require('form-data');

const downloadImage = require("./download.js");
const stream = axios.create({
    responseType: 'stream'
});

const client = axios.create({
    timeout: 100000
});

const strapi = axios.create({
    baseURL: "http://localhost:1337",
    timeout: 10000
});



function getContent(url) {
    return client.get(url);
}

async function getDataFromURL(url, selector, attr = false) {
    try {
        const res = await getContent(url);
        console.log(res.status, res.statusText)
    
        // get category urls
        let $ = cheerio.load(res.data, {decodeEntities: false})
        let html = $(selector);
        let title = $(".page-item-title-single > h1").html();

        if (!attr)
            return { html: html, title: title }
        const attrs = [];
        html.map((i, item) => attrs.push(item.attribs[attr]))
        return attrs
    } catch (err) {
        console.log("Error:", err)
    }
}



// postlarin linklerini file'a yazmag ucun bu funksiyani cagir :P
// (async () => {
//     // get category urls
//     const url = "https://bakuresearchinstitute.org/";
//     const links = await getDataFromURL(url, "#menu-main_menu-1 > li.menu-item > a", "href");

//     // get post links
//     let postLinks = []
//     for (let i = 0; i < links.length; i++) {
//         let plks = await getDataFromURL(links[i], ".bri-post-content > a", "href");
//         postLinks.push(...plks)
//     }

//     const json = JSON.stringify(postLinks);

//     fs.writeFile("postLinks.json", json, 'utf8', function (err) {
//         if (err) {
//             console.log("An error occured while writing JSON Object to File.");
//             return console.log(err);
//         }
        
//         console.log("JSON file has been saved.");
//     });

// })


function makeQuery(data) {
    const content = data.content;
    const title = data.title;
    
    const input = {
        title: JSON.parse(JSON.stringify(title)),
        description: "",
        content: `${content.replace(/"/g, "'").replace(/\/n/g, "")}`,
        draft: true,
        language: "5d611cd73a0dc8301ec1f98e"
    }
    return input;
}
    
(async () => {

    try {
        for (let i = 0; i < postsJson.length; i++) {
            let grabbedContent = await getDataFromURL(postsJson[i], ".entry-content.about-page");
            let html = grabbedContent.html;
            let title = grabbedContent.title;

            const ch = require('cheerio');

            let content = ch.load(html.html());
            contentString = html.html();

            let imgs = content("img")
            console.log('images length: ', imgs.length);

            imgs.map((i, img) => {
                console.log("img", i)
                const filename = img.attribs.src.split("/").pop();
                client({
                    method: 'get',
                    url: img.attribs.src,
                    responseType:'stream'
                })
                .then(res => {
                    let stream = res.data.pipe(fs.createWriteStream(`./images/${filename}`));
                    stream.on('finish', function () { 
                        uploadFileAndGetUrl(filename, img.attribs.src, function(paths){
                            contentString = html.html().replace(paths.oldPath, paths.newPath);
                            contentString = contentString.replace("srcset=", "disabled_srcset=");
                        });
                        if(i+1 == imgs.length){
                            createPost(contentString, title)
                        }
                    });
                })
            });

            if(imgs.length == 0){
                createPost(contentString, title)
            }
        }
    } catch (err) {
        console.log("error async", err)
    }

})()

function createPost(contentString, title) {
    console.log(title, "************`title")
    var turndownService = new TurndownService();
    var markdown = turndownService.turndown(contentString);
    
    strapi.post("/posts", makeQuery({content: markdown, title: title}));
}

function uploadFileAndGetUrl(filename, oldPath, callback){
    var bodyFormData = new FormData();
    pathToFile = "images/" + filename;
    imageFile = fs.createReadStream(pathToFile);
    bodyFormData.append('files', imageFile); 

    result = client({
        method: "post",
        url: "http://localhost:1337/upload",
        headers: {
            "Content-Type": `multipart/form-data; boundary=${bodyFormData._boundary}`
        },
        data: bodyFormData
    }).then(res => {
        callback({ oldPath: oldPath, newPath: res.data[0].url });
    }).catch(err => {
        console.log(err, "err")
    })
}





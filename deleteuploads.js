const axios = require("axios").default;
const cheerio = require("cheerio");
const fs = require("fs");
const filesJson = require("./deletefiles.json");
const TurndownService = require("turndown");
const FormData = require("form-data");

const client = axios.create({
  timeout: 1000000
});

var myArgs = process.argv.slice(2);
let currentFileIndex = parseInt(myArgs[0]);
let currentFile = filesJson[currentFileIndex];

for (i = 0; i < filesJson.length; i++) {
  console.log("deleting file: " + i + " - " + filesJson.length);
  client({
    method: "DELETE",
    url:
      "https://admin.bakuresearchinstitute.org/upload/files/" +
      filesJson[i]._id,
    responseType: "stream"
  }).then(res => {
    console.log("File deleted. ");
  });
}
